
********************
* Module Functions *
********************
This module allows a Jaiku stream to be cleanly added in a block
The Full Stream, User Info, and the current presence information are supported.

****************
* Installation *
****************
Just drop it into the normal place and turn it on via Administer > Site building > Modules.
Before the module can be fully used, it must first be configured in Administer > Site configuration > Jaiku Settings.

Usage:
Configure what to display in the block configuration panel.
Configuration differs depending on the type of stream to display, as set above.

*******************
* IMPORTANT NOTE! *
*******************
Because this module uses JSON to interact with the Jaiku API, it requires PHP 5.2 or later.  If you have the JSON PECL extension installed and wish to use this module, simply remove line 7 in the jaiku.info file and enable the module as you would any other module.  Note that whenever you upgrade this extention, you will have to make this change again.
